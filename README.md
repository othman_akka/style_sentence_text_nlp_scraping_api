#Style_sentence_text_NLP
This lib uses two Natural Language Processing (<a href="https://spacy.io">Spacy</a> &amp; <a href="https://www.nltk.org">NLTK</a>) and a online word-finding query engine for developers called <a href="https://www.datamuse.com/api">datamuse</a> as base to rewrite texts.

##Style_sentence_text_NLP
#### First step, install python dependencies
<pre>pip install -r requirements.txt</pre>

#### Second step, install spacy en support
<pre>python -m spacy download en <br>or <br>python -m spacy download en_core_web_lg</pre>


#### Third step, install NLTK corpora
Run this code in any python file or python terminal
<pre>import nltk<br>nltk.download()</pre>
After that select `all-corpora` and download it

#### fourth step, test
<pre>python server.py</pre>







